export interface Usuarios {
  ok:       boolean;
  usuarios: Usuario[];
  uid:      string;
  total:    number;
}

export interface Usuario {
  _id:            string;
  nombre:         string;
  email:          string;
  fecha_creacion: Date;
  role:           string;
  estado:         boolean;
  google:         boolean;
  img?:           string;
}
