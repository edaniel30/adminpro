export interface LoginForm {
  email:      string;
  password:   string;
  reemember:  boolean;
}

