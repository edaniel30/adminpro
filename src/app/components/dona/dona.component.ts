import { Component, Input, OnInit } from '@angular/core';
import { ChartData } from 'chart.js';

@Component({
  selector: 'app-dona',
  templateUrl: './dona.component.html',
  styles: [
  ]
})
export class DonaComponent{

  datasets: any[] = [];
  doughnutChartLabels: string[] = [];

  // Doughnut
  @Input() titulo: string = 'Sin titulo';
  @Input('data')  doughnutChartData: ChartData<'doughnut'> = {
    labels: this.doughnutChartLabels,
    datasets: this.datasets
  };


}

