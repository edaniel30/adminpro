import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: [
  ]
})
export class PromesasComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    // const promesa = new Promise( ( resolve, reject ) => {

    //   if ( true ){
    //     resolve('Hola mundo');
    //   } else {
    //     reject('Algo salio mal')
    //   }

    // });

    // promesa.then( ( msg ) => {
    //   console.log(msg);
    // })
    // .catch( error => {
    //   console.log('error en mi promesa', error);
    // })

    // console.log('Fin promesa');

    this.getUsuarios().then( usuarios => {
      console.log(usuarios);
    })


  }

  getUsuarios(): Promise<any> {

    const promesa = new Promise( resolve => {
      fetch('https://reqres.in/api/users')
        .then( resp => resp.json() )
        .then( body => console.log( body.data ));
    });

    return promesa;

  }
}
