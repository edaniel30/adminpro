import { Component, OnDestroy } from '@angular/core';
import { Observable, interval, Subscription } from 'rxjs';
import { retry, take, map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [
  ]
})
export class RxjsComponent implements OnDestroy {

  intervalSubs!: Subscription;

  constructor() {

    // this.retornaObservable().pipe(
    //   retry( 1 )
    // ).subscribe(
    //   valor => console.log('Subs:', valor),
    //   error => console.warn('Error:', error),
    //   () => console.info('Obs terminado')
    //   );

    this.intervalSubs = this.retornaIntervalo()
      .subscribe( console.log )

  }

  ngOnDestroy(): void {
    if ( this.intervalSubs ){
      this.intervalSubs.unsubscribe();
    }
  }

  retornaIntervalo () {

    const intervalo$ = interval( 500 )
      .pipe(
        map( valor => { return valor + 1; }),
        filter( valor =>  (valor % 2 === 0) ? true: false ),
        // take(10),
      )

    return intervalo$;
  }

  retornaObservable (): Observable<number> {

    let i = -1;
    const obs$ = new Observable<number> ( observer => {

      const intervalo = setInterval( () => {

        i++;
        observer.next(i);

        if ( i === 5 ){
          clearInterval( intervalo );
          observer.complete();
        }
        if ( i == 2 ){
          observer.error('i llego al valor de 2');
        }

      }, 1000)

    });

    return obs$;
  }

}
