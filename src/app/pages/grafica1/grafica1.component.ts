import { Component } from '@angular/core';
import { ChartData, ChartType, Color } from 'chart.js';

@Component({
  selector: 'app-grafica1',
  templateUrl: './grafica1.component.html',
  styles: [
  ]
})
export class Grafica1Component {

  // Doughnut
  labels1: string[] = [ 'Download Sales', 'In-Store Sales', 'Mail-Order Sales' ];
  data1: ChartData<'doughnut'> = {
    labels: this.labels1,
    datasets: [{ data: [ 350, 450, 100 ], backgroundColor: ['#d13537', '#FFB414', '#FF5800']}],
  };


}
