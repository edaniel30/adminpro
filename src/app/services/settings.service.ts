import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private linkTheme = document.querySelector('#theme');
  defaultTheme = './assets/css/colors/default-dark.css'

  constructor() {

    const theme = localStorage.getItem('theme') || this.defaultTheme;
    this.linkTheme?.setAttribute('href', theme);
  }

  changeTheme( theme: string) {

    const url = `./assets/css/colors/${theme}.css`

    this.linkTheme?.setAttribute('href', url);
    localStorage.setItem('theme', url);

    this.checkCurrentTheme();

  }

  checkCurrentTheme(): void {

    const links = document.querySelectorAll('.selector');

    links.forEach( element => {

      element.classList.remove('working');
      const btnTheme = element.getAttribute('data-theme');
      const btnThemeURL = `./assets/css/colors/${btnTheme}.css`;
      const currentTheme = this.linkTheme?.getAttribute('href');

      if ( btnThemeURL === currentTheme ){
        element.classList.add('working');
      }
    });
  }
}
