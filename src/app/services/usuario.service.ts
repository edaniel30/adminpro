import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { RegisterForm } from '../interfaces/register-form.interface';
import { LoginForm } from '../interfaces/login-form.interface';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';

declare const gapi:any;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private baseURL = environment.baseURL;
  auth2: any = null;

  constructor(  private http: HttpClient,
                private router: Router,
                private ngZone: NgZone ) {
    this.googleInit();
  }

  googleInit(){

    return new Promise<void>( resolve => {
      gapi.load('auth2', () => {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        this.auth2 = gapi.auth2.init({
          client_id: '537323217097-2u8csu8e594mim978v0k3f3lgdlrbrkj.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin',
        });

        resolve();
      });
    })
  }

  logout(){
    localStorage.removeItem('token');

    this.auth2.signOut().then(() => {

      this.ngZone.run(() => {
        this.router.navigateByUrl('/login');
      })
    });
  }

  validarToken(): Observable<boolean> {
    const token  = localStorage.getItem('token') || '';
    const url = `${this.baseURL}/login/renew`;
    return this.http.get( url, {headers:{
      'x-token': token
    }}).pipe(
      tap( (resp: any) => {
        localStorage.setItem('token', resp.token)
      }),
      map( resp => true),
      catchError( error => of(false) )
    )
  }

  crearUsuario( formData: RegisterForm ){

    const url = `${this.baseURL}/usuarios`;
    return this.http.post( url, formData )
      .pipe(
        tap( (resp: any) => {
          localStorage.setItem('token', resp.token);
        })
      )

  }

  login ( formData: LoginForm ){
    const url = `${this.baseURL}/login`;
    return this.http.post( url, formData )
      .pipe(
        tap( (resp: any) => {
          localStorage.setItem('token', resp.token);
        })
      )
  }

  loginGoogle ( token: string ){
    const url = `${this.baseURL}/login/google`;
    return this.http.post( url, { token } )
      .pipe(
        tap( (resp: any) => {
          localStorage.setItem('token', resp.token);
        })
      )
  }
}
